import { Module, HttpModule } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt'
import { CallbackController } from './callback.controller';
import { CallbackService } from './callback.service';

@Module({
  imports: [
    HttpModule,
    JwtModule.registerAsync({
      useFactory: () => ({ 
        publicKey: process.env.PUBLIC_KEY,
        privateKey: process.env.PRIVATE_KEY,
        signOptions: {
          algorithm: 'RS256',
          issuer: process.env.ISSUER,
          ...process.env.EXPIRES_IN && {
            expiresIn: process.env.EXPIRES_IN,
          }
        },
      }),
    })
  ],
  controllers: [CallbackController],
  providers: [CallbackService]
})
export class CallbackModule {}
