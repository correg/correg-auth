import { HttpService, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CMUOAuthResponse, CMUUserInfo } from './callback.dto'

@Injectable()
export class CallbackService {
  constructor(
    private readonly httpService: HttpService,
    private readonly jwtService: JwtService,
  ) {}

  async getTokenFromOAuth(code: string, redirectUri: string): Promise<CMUOAuthResponse> {
    const { data } = await this.httpService
      .post<CMUOAuthResponse>('https://oauth.cmu.ac.th/v1/GetToken.aspx', {}, {
        params: {
          code,
          redirect_uri: redirectUri,
          client_id: process.env.CMUOAUTH_CLIENT_ID,
          client_secret: process.env.CMUOAUTH_SECRET,
          grant_type: 'authorization_code',
        }
      })
      .toPromise()

    return data
  }

  async getUserInfo(accessToken: string): Promise<CMUUserInfo> {
    const { data } = await this.httpService
      .get<CMUUserInfo>('https://misapi.cmu.ac.th/cmuitaccount/v1/api/cmuitaccount/basicinfo', {
        headers: {
          authorization: `bearer ${accessToken}`
        }
      }).toPromise()

    return data
  }

  signBearerToken(user: CMUUserInfo) {
    return {
      type: 'bearer',
      token: this.jwtService.sign(user)
    }
  }
}
