import { Controller, Post, Get, Body } from '@nestjs/common';
import { CallbackService } from './callback.service'
import { CMUUserInfo } from './callback.dto'

@Controller('callback')
export class CallbackController {
  constructor(
    private readonly callbackService: CallbackService,
  ) {}

  @Post()
  async callback(
    @Body('code') code: string,
    @Body('redirect_uri') redirectUri: string,
  ) {
    const { access_token } = await this.callbackService.getTokenFromOAuth(code, redirectUri)

    const user = await this.callbackService.getUserInfo(access_token)

    return this.callbackService.signBearerToken(user)
  }

  @Get()
  sign() {
    if (process.env.NODE_ENV === 'production') {
      return
    }

    const user: CMUUserInfo = {
      cmuitaccount_name: "correg",
      cmuitaccount: "correg@cmu.ac.th",
      student_id: "600610773",
      prename_id: "MR",
      prename_TH: "นาย",
      prename_EN: "Mr.",
      firstname_TH: "โคเร็ค",
      firstname_EN: "CORREG",
      lastname_TH: "หวังว่าคุณจะดีขึ้นกว่าวันนี้นะ",
      lastname_EN: "I HOPE YOU WILL BETTER THAN THIS DAY",
      organization_code: "06",
      organization_name_TH: "คณะวิศวกรรมศาสตร์",
      organization_name_EN: "Faculty of Engineering",
      itaccounttype_id: "StdAcc",
      itaccounttype_TH: "นักศึกษาปัจจุบัน",
      itaccounttype_EN: "Student Account"
    }

    return this.callbackService.signBearerToken(user)
  }
}
