import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CallbackModule } from './callback/callback.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    CallbackModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
